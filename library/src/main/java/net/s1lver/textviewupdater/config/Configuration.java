package net.s1lver.textviewupdater.config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import android.os.Handler;
import android.os.Looper;

/**
 * 
 * @author Andriy Hulyk {@literal <andriy.hulyk@gmail.com>}
 *
 */
public class Configuration {

	private ExecutorService taskExecutor;

	private Handler handler;
	
	private boolean resetBeforeLoading;
	
	private boolean enableLog;

	public ExecutorService getTaskExecutor() {
		return taskExecutor;
	}

	public Handler getHandler() {
		return handler;
	}

	public boolean resetBeforeLoading() {
		return resetBeforeLoading;
	}
	
	public boolean enableLog() {
		return enableLog;
	}
	
	private Configuration(Builder builder) {
		if (builder.taskExecutor != null) {
			taskExecutor = builder.taskExecutor;
		} else {
			taskExecutor = ConfigurationFactory.createTaskExecutor(
					builder.threadPoolSize, builder.threadPriority,
					builder.threadPoolName);
		}
		if (builder.handler != null) {
			handler = builder.handler;
		} else {
			handler = ConfigurationFactory.createHandler();
		}
		this.resetBeforeLoading = builder.resetBeforeLoading;
		this.enableLog = builder.enableLog;
	}

	public static class Builder {

		private static final int THREAD_POOL_SIZE = 3;

		private static final String THREAD_POOL_NAME = "tv-loader-pool-";

		private static final int THREAD_PRIORITY = Thread.NORM_PRIORITY - 2;

		private int threadPoolSize = THREAD_POOL_SIZE;
		
		private String threadPoolName = THREAD_POOL_NAME;
		
		private int threadPriority = THREAD_PRIORITY;
		
		private ExecutorService taskExecutor;
		
		private Handler handler;
		
		private boolean resetBeforeLoading;
		
		private boolean enableLog;

		public Builder setThreadPoolSize(int threadPoolSize) {
			this.threadPoolSize = threadPoolSize;
			return this;
		}

		public Builder setThreadPoolName(String threadPoolName) {
			this.threadPoolName = threadPoolName;
			return this;
		}

		public Builder setThreadPriority(int threadPriority) {
			this.threadPriority = threadPriority;
			return this;
		}

		public Builder setTaskExecutor(ExecutorService taskExecutor) {
			this.taskExecutor = taskExecutor;
			return this;
		}

		public Builder setHandler(Handler handler) {
			this.handler = handler;
			return this;
		}

		public Builder setResetBeforeLoading(boolean resetBeforeLoading) {
			this.resetBeforeLoading = resetBeforeLoading;
			return this;
		}
		
		public Builder setEnableLog(boolean enableLog) {
			this.enableLog = enableLog;
			return this;
		}
		
		public Configuration build() {
			return new Configuration(this);
		}

	}

	private static class ConfigurationFactory {

		public static Handler createHandler() {
			return new Handler(Looper.getMainLooper());
		}

		public static ExecutorService createTaskExecutor(int treadPoolSize,
				int threadPriority, String threadPoolPrefix) {
			return new ThreadPoolExecutor(0, treadPoolSize, 5L,
					TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(),
					createThreadFactory(threadPriority, threadPoolPrefix));
		}

		private static ThreadFactory createThreadFactory(int priority,
				String namePrefix) {
			return new DefaultThreadFactory(priority, namePrefix);
		}

		private static class DefaultThreadFactory implements ThreadFactory {

			private AtomicInteger threadNumber = new AtomicInteger(1);
			private ThreadGroup group = Thread.currentThread().getThreadGroup();
			private String namePrefix;
			private int priority;

			public DefaultThreadFactory(int priority, String namePrefix) {
				this.namePrefix = namePrefix + "thread-";
				this.priority = priority;
			}

			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(group, r, namePrefix
						+ threadNumber.getAndIncrement(), 0);
				if (thread.isDaemon())
					thread.setDaemon(false);
				thread.setPriority(priority);
				return thread;
			}

		}

	}
}
