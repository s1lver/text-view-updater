package net.s1lver.textviewupdater.core;

/**
 * <pre>
 * The <code>Worker</code> interface  should implemented by any class whose instances are needed
 * to do some work for getting String result.
 * </pre>
 * 
 * @author Andriy Hulyk {@literal <andriy.hulyk@gmail.com>}
 *
 */
public interface Worker {

	String doWork();

}
