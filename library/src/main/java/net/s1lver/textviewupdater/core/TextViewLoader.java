package net.s1lver.textviewupdater.core;

import android.util.Log;
import android.widget.TextView;

import net.s1lver.textviewupdater.config.Configuration;

/**
 * Singleton for async loading data and displaying in the {@link android.widget.TextView}
 * </br>
 * If method {@link #loadConfig(net.s1lver.textviewupdater.config.Configuration)} is not called the default config will be used.
 * 
 * @author Andriy Hulyk {@literal <andriy.hulyk@gmail.com>}
 * 
 */
public enum TextViewLoader {
	
	/**
	 * Return singleton, thread-save instance of {@link net.s1lver.textviewupdater.core.TextViewLoader}
	 */
	INSTANCE;
	
	private static final String TAG = TextViewLoader.class.getSimpleName();
	
	private static final String EXEPTION_NOT_CORRECT_ARGUMENTS = "Wrong arguments were passed into loadText() method!";
	
	private static final String WARNING_NO_CONFIG_SPECIFIED = "No config specified. Use default!";
	
	private static final String WARNING_APPLYING_NEW_CONFIG = "Config has been re-loaded at Runtime!";
	
	private Configuration config;
	
	private Processor processor;
	
	public synchronized void loadConfig(Configuration config) {
		
		if (configLoaded()) {
			processor.stop();
			Log.w(TAG, WARNING_APPLYING_NEW_CONFIG);
		} 
		
		this.config = config;
		this.processor = new Processor(this.config);
	}
	
	private boolean configLoaded() {
		return config != null;
	}
	
	/**
	 * Main method for async loading data using {@link Worker} interface and displaying it in given {@linkplain android.widget.TextView}
	 * 
	 * @param v
	 * @param worker
	 * @see {@link Worker}, {@link android.widget.TextView}
	 */
	public void loadText(TextView v, Worker worker) {
		
		if (!configLoaded()) {
			loadConfig(new Configuration.Builder().build());
			Log.w(TAG, WARNING_NO_CONFIG_SPECIFIED);
		}
		
		if (worker == null || v == null) {
			throw new IllegalArgumentException(EXEPTION_NOT_CORRECT_ARGUMENTS);
		}
		processor.prepareTask(v, worker);
		processor.submitTask(new LoadAndDisplayTask(processor, worker, v));
	}
	
}
