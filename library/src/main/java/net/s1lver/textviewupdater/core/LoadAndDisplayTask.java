package net.s1lver.textviewupdater.core;

import android.util.Log;
import android.widget.TextView;

import net.s1lver.textviewupdater.utils.Strings;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Andriy Hulyk {@literal <andriy.hulyk@gmail.com>}
 *
 */
public class LoadAndDisplayTask implements Runnable {
	
	private static final String TAG = LoadAndDisplayTask.class.getSimpleName();

	private static final String INFO_STARTING_NEW_TASK = "Starting new Task. Thread ID: ";
	private static final String INFO_TASK_FINISHED = "Task finished and removed from pool. Thread ID: ";
	
	private Processor processor;
	
	private Worker worker;
	private TextView view;
	
	public LoadAndDisplayTask(Processor processor, Worker worker, TextView view) {
		super();
		this.worker = worker;
		this.view = view;
		this.processor = processor;
	}

	@Override
	public void run() {

		if (processor.getConfig().enableLog()) {
			Log.i(TAG, INFO_STARTING_NEW_TASK + Thread.currentThread().getName());
		}
		
		if (isTaskActual(view, worker)) {
			
			if (processor.getConfig().resetBeforeLoading()) {
				runUpdateViewTask(createUpdateTaskViewTask(null));
			}
			
			final String value = worker.doWork();
			
			ReentrantLock lock = processor.getViewLock(view);
			lock.lock();
			try {
				if (isTaskActual(view, worker)) {
					runUpdateViewTask(createUpdateTaskViewTask(value));
					processor.cancelTask(view);
				}
			} finally {
				lock.unlock();
				if (processor.getConfig().enableLog()) {
					Log.i(TAG, INFO_TASK_FINISHED + Thread.currentThread().getName());
				}
			}
		}
		
	}
	
	private Runnable createUpdateTaskViewTask(final String newValue) {
		return new Runnable() {
			
			@Override
			public void run() {
				if (view != null) {
					view.setText(Strings.nullToEmpty(newValue));
				}
			}
		};
	}

	private boolean isTaskActual(TextView v, Worker worker) {
		return processor.isTaskActual(v, worker);
	}
	
	private void runUpdateViewTask(Runnable r) {
		processor.getConfig().getHandler().post(r);
	}
	
}
