package net.s1lver.textviewupdater.core;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import android.view.View;
import android.widget.TextView;

import net.s1lver.textviewupdater.config.Configuration;

public class Processor {

	private Configuration config;
	
	private Map<TextView, Worker> taskPool = new ConcurrentHashMap<TextView, Worker>();
	private Map<TextView, ReentrantLock> viewLocks = new WeakHashMap<TextView, ReentrantLock>();
	
	public Processor(Configuration config) {
		this.config = config;
	}
	
	public Configuration getConfig() {
		return config;
	}
	
	/**
	 * <pre>
	 * Thread-safe method for putting {@link android.widget.TextView} and associated {@link Worker} to the
	 * {@link java.util.Map} for further processing.
	 * </pre>
	 * 
	 * @param v
	 * @param worker
	 */
	public void prepareTask(TextView v, Worker worker) {
		taskPool.put(v, worker);
	}
	
	/**
	 * <pre>
	 * Thread-safe method for removing {@link android.widget.TextView} and associated {@link Worker} from
	 * {@link java.util.Map}.
	 * </pre>
	 * @param v
	 * @param worker
	 */
	public void cancelTask(View v) {
		taskPool.remove(v);
	}
	
	/**
	 * <pre>
	 * @param v
	 * @param worker
	 * @return false if {@link Worker} is more not actual for associated {@link android.widget.TextView}.
	 * Otherwise true.
	 * </pre>
	 */
	public boolean isTaskActual(TextView v, Worker worker) {
		return taskPool.get(v) == worker;
	}
	
	/**
	 * Try to get cached {@link java.util.concurrent.locks.ReentrantLock} or create new one.
	 * 
	 * @param v
	 * @return {@link java.util.concurrent.locks.ReentrantLock} associated with {@link android.widget.TextView} v.
	 */
	ReentrantLock getViewLock(TextView v) {
		ReentrantLock lock = viewLocks.get(v);
		if (lock == null) {
			lock = new ReentrantLock();
			viewLocks.put(v, lock);
		}
		return lock;
	}
	
	/**
	 * Submit task for further execution.
	 * @param task
	 */
	void submitTask(LoadAndDisplayTask task) {
		config.getTaskExecutor().execute(task);
	}
	
	void stop() {
		config.getTaskExecutor().shutdown();
	}
	
}
