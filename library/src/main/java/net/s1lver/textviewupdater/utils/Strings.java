package net.s1lver.textviewupdater.utils;

/**
 *
 *  @author Andriy Hulyk {@literal <andriy.hulyk@gmail.com>}
 *
 *
 */
public class Strings {

    public static String nullToEmpty(String value) {
        if (value == null) {
            return new String("");
        } else return value;
    }

}
